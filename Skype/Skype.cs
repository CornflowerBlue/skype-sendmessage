﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace Skype
{
    public sealed class Skype
    {
        public static Chat[] GetAllActiveChats()
        {
            var chats = new List<Chat>();

            var skype = Process.GetProcessesByName("Skype");
            if (skype.Length == 0) return null;
            var chld = Win.GetAllWindows();
            foreach (var c in chld.Cast<IntPtr>())
            {
                var chatEntryControl = Win.FindWindowEx(c, new IntPtr(0), "TChatEntryControl", null);
                var chatRichEdit = Win.FindWindowEx(chatEntryControl, new IntPtr(0), "TChatRichEdit", null);
                if (chatRichEdit != IntPtr.Zero)
                {
                    chats.Add(new Chat(Win.GetControlText(c)));
                }
            }

            return chats.ToArray();
        }

        public static string GetUsername()
        {
            var skype = Process.GetProcessesByName("Skype").FirstOrDefault();
            return skype != null ? skype.MainWindowTitle.Split("-".ToCharArray())[1] : null;
        }
    }
}
