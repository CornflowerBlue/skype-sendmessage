﻿using System;
using System.Diagnostics;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace Skype
{
    public sealed class Chat
    {
        public readonly string ChatName;

        private readonly IntPtr _editHandle;
        private readonly IntPtr _chatHandle;

        public Chat(string skypeChatName)
        {
            var skype = Process.GetProcessesByName("Skype");
            if (skype.Length == 0) return;
            var chld = Win.GetAllWindows();
            foreach (var c in chld.Cast<IntPtr>()) // TConversationForm
            {
                if (Win.GetControlText(c) == skypeChatName)
                {
                    _chatHandle = Win.FindWindowEx(c, new IntPtr(0), "TChatContentControl", null);
                    _editHandle = Win.FindWindowEx(Win.FindWindowEx(c, new IntPtr(0), "TChatEntryControl", null), new IntPtr(0), "TChatRichEdit", null);

                    ChatName = skypeChatName;
                }
            }

            if(_editHandle == IntPtr.Zero || _chatHandle == IntPtr.Zero)
                throw new Exception("Invalid chat.");
        }

        public bool SendMessage(string message)
        {
            var rtn = Win.SendMessage(_editHandle, 0x000C, 0, message);
            if(rtn)
                SendEnter();
            return rtn;
        }

        public bool EditLastMessage(string newMessage)
        {
            SendUp();
            return SendMessage(newMessage);
        }

        private void SendEnter()
        {
            Win.PostMessage(_editHandle, Win.WmKeydown, (IntPtr)Win.VkEnter, IntPtr.Zero);
            Win.PostMessage(_editHandle, Win.WmChar, (IntPtr)Win.VkEnter, IntPtr.Zero);
            Win.SendMessage(_editHandle, Win.WmChar, (IntPtr)Win.VkEnter, IntPtr.Zero);
            Win.PostMessage(_editHandle, Win.WmKeyup, (IntPtr)Win.VkEnter, IntPtr.Zero);
        }

        private void SendUp()
        {
            //Win.PostMessage(_editHandle, Win.WmKeydown, (IntPtr)Win.VkUp, IntPtr.Zero);
            Win.SendMessage(_editHandle, Win.WmKeydown, (IntPtr)Win.VkUp, IntPtr.Zero);
            Win.PostMessage(_editHandle, Win.WmKeyup, (IntPtr)Win.VkUp, IntPtr.Zero);
        }

        public override string ToString()
        {
            return ChatName;
        }
    }
}