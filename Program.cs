﻿using System;
using System.Collections.Generic;
using SkypeTest.Spammer;

namespace SkypeTest
{
    class Program
    {
        private static readonly List<ISpam> Methods = new List<ISpam>
        {
            new Flash(), 
            new TypeWriter(), 
            new Thump()
        };

        static void Main()
        {
            Console.WriteLine("Please select your chat");
            Console.Title = Skype.Skype.GetUsername();
            var chats = Skype.Skype.GetAllActiveChats();
            for (var i = 0; i < chats.Length; i++)
            {
                Console.WriteLine("[{0}] {1}", i, chats[i]);
            }
            var chat = chats[int.Parse(Console.ReadLine())];
            Console.Clear();
            Console.WriteLine("Please select method");
            for (var i = 0; i < Methods.Count; i++)
            {
                Console.WriteLine("[{0}] {1}", i, Methods[i].Text());
            }
            var method = Methods[int.Parse(Console.ReadLine())];

            method.Run(chat);
        }


    }
}
