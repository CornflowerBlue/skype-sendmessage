﻿using System;
using System.Threading;

namespace SkypeTest.Spammer
{
    class Flash : ISpam
    {
        public void Run(Skype.Chat chat)
        {
            Console.Clear();
            Console.WriteLine("Please enter message one");
            var msg1 = Console.ReadLine();
            Console.WriteLine("Please enter message two");
            var msg2 = Console.ReadLine();

            chat.SendMessage(msg1);

            while (true)
            {
                chat.EditLastMessage(msg2);
                Thread.Sleep(100);
                chat.EditLastMessage(msg1);
                Thread.Sleep(100);
            }
// ReSharper disable once FunctionNeverReturns
        }

        public string Text()
        {
            return "Flashy text";
        }
    }
}
