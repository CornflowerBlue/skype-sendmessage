﻿using System.Threading;

namespace SkypeTest.Spammer
{
    class Thump : ISpam
    {
        public void Run(Skype.Chat chat)
        {
            chat.SendMessage(":)");

            while (true)
            {
                chat.EditLastMessage("(devil)");
                Thread.Sleep(100);
                chat.EditLastMessage(":)");
                Thread.Sleep(100);
            }
// ReSharper disable once FunctionNeverReturns
        }

        public string Text()
        {
            return "Thumb Spam";
        }
    }
}
