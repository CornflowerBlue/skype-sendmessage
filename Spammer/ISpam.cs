﻿using Skype;

namespace SkypeTest.Spammer
{
    interface ISpam
    {
        void Run(Chat chat);
        string Text();
    }
}
