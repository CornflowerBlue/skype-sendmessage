﻿using System;
using System.Globalization;
using System.Text;
using System.Threading;

namespace SkypeTest.Spammer
{
    class TypeWriter : ISpam
    {
        public void Run(Skype.Chat chat)
        {
            Console.Clear();
            Console.WriteLine("Please enter text to spam");
            var msg = Console.ReadLine();

            if (msg == null) return;
            var letters = msg.ToCharArray();
            chat.SendMessage(letters[0].ToString(CultureInfo.InvariantCulture));

            var curPos = 0;
            var fwr = true;
            while (true)
            {
                var sb = new StringBuilder();

                for (int i = 0; i < letters.Length; i++)
                {
                    if (i <= curPos)
                        sb.Append(letters[i].ToString(CultureInfo.InvariantCulture));
                    else
                    {
                        sb.Append(RandomString(letters.Length - i));
                        break;
                    }
                }
                if (fwr)
                    curPos++;
                else
                    curPos--;

                if (curPos > letters.Length)
                {
                    fwr = false;
                    curPos = 0;
                    Thread.Sleep(1000);
                }
                else if (curPos < 0)
                {
                    fwr = true;
                    curPos = 0;
                }

                Thread.Sleep(50);
                chat.EditLastMessage(sb.ToString());
            }
// ReSharper disable once FunctionNeverReturns
        }

        static string RandomString(int length)
        {
            const string keys = "qwertyuiopasdfghjklzxcvbnm123456789";
            var sb = new StringBuilder();

            while (sb.Length < length)
                sb.Append(keys[new Random(new Random().Next(0, int.MaxValue / 2) + sb.Length - length).Next(keys.Length)]);
            //sb.Append("#");

            return sb.ToString();
        }

        public string Text()
        {
            return "Type writer";
        }
    }
}
